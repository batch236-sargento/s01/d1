package com.zuitt.example;

import java.util.Scanner;

public class s1a1 {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = userInput.next();

        System.out.println("Last Name:");
        String lastName = userInput.next();

        System.out.println("First Subject Grade: ");
        double firstSubject = userInput.nextDouble();

        System.out.println("Second Subject Grade: ");
        double secondSubject = userInput.nextDouble();

        System.out.println("Third Subject Grade: ");
        double thirdSubject = userInput.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + ".");

        double grades = firstSubject + secondSubject + thirdSubject;
        int noOfSubjects = 3;

        double gradeAverage = grades / noOfSubjects;
        System.out.println("Your grade average is: " + gradeAverage);
    }
}
